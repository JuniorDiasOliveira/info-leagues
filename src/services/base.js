const TOKEN = "HOLCAStI6Z0OfdoPbjdSg5b41Q17w2W5P4WuoIBdC66Z54kUEvGWPIe33UYC";
const BASE_URL = "https://soccer.sportmonks.com/api/v2.0";

export const createFetch = async (endpoint, config = {}) => {
  const url = `${BASE_URL + endpoint}?api_token=${TOKEN}`;
  return fetch(url, config).then(response => response.json());
};
