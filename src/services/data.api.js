import { createFetch } from "./base";

export const getLeagues = async () => {
  return await createFetch("/leagues");
};

export const getTeams = async () => {
  return await createFetch("/teams");
};

export const getSeasons = async () => {
  return await createFetch("/seasons");
};

export const getTeamsBySeason = async id => {
  return await createFetch(`/teams/season/${id}`);
};

export const getSeasonStandings = async id => {
  return await createFetch(`/standings/season/${id}`);
};
