import React from "react";
import PropTypes from "prop-types";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { splitEvery } from "ramda";
import { DialogContent } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  buttonMargin: {
    margin: theme.spacing(1),
    border: "1px solid transparent"
  },
  buttonGroup: {
    boxSizing: "border-box"
  }
}));

function DefaultDialog(props) {
  const classes = useStyles();
  const { onClose, open, title, data, onSelected } = props;
  const matrix = splitEvery(5, data);
  return (
    <Dialog onClose={onClose} aria-labelledby="seasons-dialog" open={open}>
      <DialogTitle id="seasons-dialog-title">{title}</DialogTitle>
      <DialogContent>
        {matrix.map(item => (
          <ButtonGroup
            color="secondary"
            aria-label="outlined secondary button group"
            key={Math.random()}
          >
            {item.map(({ id, name }) => (
              <Button
                key={id}
                className={classes.buttonMargin}
                onClick={() => onSelected(id)}
              >
                {name}
              </Button>
            ))}
          </ButtonGroup>
        ))}
      </DialogContent>
    </Dialog>
  );
}

DefaultDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  onSelected: PropTypes.func.isRequired
};

export default DefaultDialog;
