import React from "react";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import { Grow } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  heroContent: {
    padding: theme.spacing(8, 0, 6)
  }
}));

const Hero = ({ title, subtitle }) => {
  const classes = useStyles();
  return (
    <Container maxWidth="sm" component="main" className={classes.heroContent}>
      <Typography
        component="h1"
        variant="h2"
        align="center"
        color="primary"
        gutterBottom
      >
        {title}
      </Typography>
      <Grow in={!!subtitle}>
        <Typography variant="h5" align="center" color="primary" component="p">
          {subtitle}
        </Typography>
      </Grow>
    </Container>
  );
};
Hero.defaultProps = {
  title: "Blip",
  subtitle: ""
};
export default Hero;
