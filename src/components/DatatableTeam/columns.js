export const columns = [
  {
    id: "position",
    numeric: false,
    label: "Position"
  },
  { id: "name", numeric: false, disablePadding: false, label: "Team Name" },
  { id: "played", numeric: true, disablePadding: false, label: "Played" },
  { id: "won", numeric: true, disablePadding: false, label: "Won" },
  { id: "draw", numeric: true, disablePadding: false, label: "Draw" },
  { id: "lost", numeric: true, disablePadding: false, label: "Lost" },
  { id: "goal", numeric: true, disablePadding: false, label: "Goal" },
  {
    id: "difference",
    numeric: false,
    disablePadding: false,
    label: "Difference"
  },
  { id: "points", numeric: true, disablePadding: false, label: "Points" }
];
