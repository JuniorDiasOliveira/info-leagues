export const mapDataToTable = data => {
  return data.map(({ position, points, overall, total, team_name }) => {
    return {
      position,
      points,
      name: team_name,
      played: overall.games_played,
      won: overall.won,
      draw: overall.draw,
      lost: overall.lost,
      goal: overall.goals_scored,
      difference: total.goal_difference
    };
  });
};
