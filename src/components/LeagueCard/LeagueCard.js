import React, { useState } from "react";
import { makeStyles, withTheme } from "@material-ui/core/styles";
import {
  Box,
  Button,
  CardHeader,
  Typography,
  CardMedia,
  CardContent,
  CardActions,
  Card
} from "@material-ui/core";
import { getSeasons } from "../../services/data.api";
import DefaultDialog from "../Dialog/DefaultDialog";

const useStyles = makeStyles(theme => {
  return {
    cardHeader: {
      backgroundColor: theme.palette.niceBlue,
      color: theme.palette.grey[200]
    },
    cardBody: {
      display: "flex",
      justifyContent: "center",
      alignItems: "baseline",
      marginBottom: theme.spacing(2),
      width: "auto",
      textAlign: "center"
    },
    media: {
      height: "180px",
      width: "100%",
      objectFit: "contain"
    },
    cardBackColor: {
      backgroundColor: theme.palette.grey[300]
    },
    seasonLinks: {
      color: theme.palette.primary.dark
    }
  };
});

const LeagueCard = ({ id, title, logo, buttonText, selectedSeason, theme }) => {
  const classes = useStyles();
  const [seasonsByTeam, setSeasonByTeam] = useState([]);
  const [modalOpen, setModalOpen] = React.useState(false);

  const handleDialogClose = isItOk => {
    setModalOpen(isItOk);
  };

  const fetchSeasons = async () => {
    const { data } = await getSeasons();
    const mapDateList = data
      .filter(x => x.league_id === id)
      .map(({ name, id }) => ({ name, id }));
    setSeasonByTeam(mapDateList);
  };

  return (
    <>
      <Card>
        <CardHeader
          title={title}
          subheader=""
          titleTypographyProps={{ align: "center" }}
          subheaderTypographyProps={{ align: "center" }}
          className={classes.cardHeader}
        />
        <CardContent className={classes.cardBackColor}>
          <div className={classes.cardBody}>
            <CardMedia className={classes.media} image={logo} component="img" />
          </div>
        </CardContent>
        <CardActions className={classes.cardBackColor}>
          <Button
            fullWidth
            variant="contained"
            color="secondary"
            onClick={() => fetchSeasons().finally(() => setModalOpen(true))}
          >
            <Typography color="primary" component="span">
              <Box fontWeight={700}>{buttonText}</Box>
            </Typography>
          </Button>
        </CardActions>
      </Card>

      <DefaultDialog
        open={modalOpen}
        onClose={() => handleDialogClose(false)}
        onSelected={async id => {
          const everyOk = await selectedSeason(id);
          handleDialogClose(!everyOk);
        }}
        title="Choose from available seasons below"
        data={seasonsByTeam}
      />
    </>
  );
};

export default withTheme(LeagueCard);
