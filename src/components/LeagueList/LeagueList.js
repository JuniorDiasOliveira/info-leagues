import React, { useState } from "react";
import LeagueCard from "../LeagueCard/LeagueCard";
import { Grid, Box } from "@material-ui/core";
import { getSeasonStandings } from "../../services/data.api";
import { path, compose, ifElse, isEmpty, isNil, not } from "ramda";
import { mapDataToTable } from "../DatatableTeam/helpers";
import SnackbarMessage from "../SnackbarMessage/SnackbarMessage";

const pathToData = path(["data", 0, "standings", "data"]);
const dataExtractor = compose(mapDataToTable, pathToData);
const hasData = compose(not, isNil, pathToData);
const validateData = ifElse(hasData, dataExtractor, () => []);

const LeagueList = ({ data, onSelected }) => {
  const [showMessage, setShowMessage] = useState(false);

  const hideMessageAfterWhile = () => {
    setShowMessage(false);
  };

  const onSeasonSelected = async id => {
    const teamsData = await getSeasonStandings(id);
    const data = validateData(teamsData);
    if (isEmpty(data)) {
      setShowMessage(true);
      setTimeout(hideMessageAfterWhile, 5000);
      return false;
    }
    onSelected(data);
    return true;
  };

  return (
    <Box>
      <Grid container spacing={5} alignItems="flex-end">
        {data.map(({ id, name, logo_path }) => (
          <Grid key={name} item xs={12} sm={12} md={4}>
            <LeagueCard
              id={id}
              title={name}
              logo={logo_path}
              buttonText="Select this league"
              selectedSeason={onSeasonSelected}
            />
          </Grid>
        ))}
      </Grid>
      <SnackbarMessage
        open={showMessage}
        onClose={hideMessageAfterWhile}
        severity="info"
      >
        <b>No data found</b>
        Please selected another season
      </SnackbarMessage>
    </Box>
  );
};

export default LeagueList;
