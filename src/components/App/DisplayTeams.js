import React from "react";
import { Box, Container } from "@material-ui/core";
import { isEmpty } from "ramda";
import DatatableTeam from "../DatatableTeam/DatatableTeam";
import Transition from "./Transition";

const DisplayTeams = ({ teamList, onEntered }) => {
  return (
    <Transition
      type="slide"
      condition={!isEmpty(teamList)}
      onEntered={() => onEntered(true)}
    >
      <Box mb={3}>
        <Container maxWidth="md" component="div">
          <DatatableTeam data={teamList} />
        </Container>
      </Box>
    </Transition>
  );
};

export default DisplayTeams;
