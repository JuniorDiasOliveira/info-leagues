import React from "react";
import { Button, Typography, Container, Box } from "@material-ui/core";

export default function ResetButton({ onClick }) {
  return (
    <Box mb={6}>
      <Container maxWidth="md" component="div">
        <Button variant="contained" color="secondary" onClick={() => onClick()}>
          <Typography color="primary" component="span">
            <Box fontWeight={700}>Reset</Box>
          </Typography>
        </Button>
      </Container>
    </Box>
  );
}
