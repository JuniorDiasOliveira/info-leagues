import React from "react";
import { Slide, Grow } from "@material-ui/core";

const slideTransition = ({ condition, onEntered, children }) => {
  return (
    <Slide
      direction="up"
      in={condition}
      timeout={1000}
      onEntered={() => onEntered(true)}
      mountOnEnter
      unmountOnExit
    >
      {children}
    </Slide>
  );
};

const growTransition = ({ condition, onEnter, children }) => {
  return (
    <Grow
      in={condition}
      onEnter={() => onEnter(false)}
      mountOnEnter
      unmountOnExit
    >
      {children}
    </Grow>
  );
};

const transitionTypes = {
  grow: growTransition,
  slide: slideTransition
};

function Transition({ type, ...props }) {
  return transitionTypes[type](props);
}

export default Transition;
