import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Hero from "../Hero/Hero";
import { Box } from "@material-ui/core";
import { getLeagues } from "../../services/data.api";
import { isEmpty, always, ifElse, not } from "ramda";
import DisplayLeagues from "./DisplayLeagues";
import DisplayTeams from "./DisplayTeams";
import ResetButton from "./ResetButton";

const classes = makeStyles(() => ({
  root: {
    flexGrow: 1
  }
}));

const subMessages = [
  "Below you can select any league returned by the API",
  "Now you can see all teams from the selected season"
];

const getMessage = ifElse(not, always(subMessages[0]), always(subMessages[1]));

const App = () => {
  const [leagueList, setLeagueList] = useState([]);
  const [teamList, setTeamList] = useState([]);
  const [resetButton, showResetButton] = useState(false);

  const displayLeagueProps = {
    leagueList,
    teamList,
    onEnter: showResetButton,
    onSelected: setTeamList
  };

  const displayTeamsProps = {
    teamList,
    onEntered: showResetButton
  };

  const fetchLeagues = () => {
    getLeagues()
      .then(({ data }) => setLeagueList(data))
      .then(() => setTeamList([]));
  };

  useEffect(() => {
    fetchLeagues();
  }, []);

  return (
    <Box className={classes.root}>
      <Hero
        title="BLIP Test Challenge"
        subtitle={getMessage(!isEmpty(teamList))}
      />
      {!isEmpty(leagueList) && <DisplayLeagues {...displayLeagueProps} />}
      {!isEmpty(teamList) && <DisplayTeams {...displayTeamsProps} />}
      {resetButton && <ResetButton onClick={fetchLeagues} />}
    </Box>
  );
};

export default App;
