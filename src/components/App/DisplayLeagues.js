import React from "react";
import { isEmpty } from "ramda";
import { Box, Container } from "@material-ui/core";
import LeagueList from "../LeagueList/LeagueList";
import Transition from "./Transition";

export default function DisplayLeagues({
  leagueList,
  teamList,
  onEnter,
  onSelected
}) {
  return (
    <Transition
      type="grow"
      condition={isEmpty(teamList)}
      onEnter={() => onEnter(false)}
    >
      <Box mb={6}>
        <Container maxWidth="md" component="div">
          <LeagueList data={leagueList} onSelected={onSelected} />
        </Container>
      </Box>
    </Transition>
  );
}
