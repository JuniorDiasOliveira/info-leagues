import { red } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#dff6f0"
    },
    secondary: {
      main: "#46b3e6"
    },
    niceBlue: "#4d80e4",
    error: {
      main: red.A400
    },
    background: {
      default: "#fff"
    }
  },
  typography: {
    fontFamily:
      '"Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue"',
    htmlFontSize: 16,
    color: "white"
  }
});

export default theme;
