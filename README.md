Blip Project Test
=============

How to run
-----------

- Download directly the project or clone it:  
`git clone git@gitlab.com:JuniorDiasOliveira/info-leagues.git`
- Go to the folder of the project and install all dependencies running `yarn install`
- To run the project just use `yarn start`, it will be ran in the `port 3000`

What could be improved
---------------

- Would be amazing if tests is implemented
- The SASS could be completely removed
- We could use Redux to remove dependency from App to share data between the components
