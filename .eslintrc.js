const isProduction = process.env.NODE_ENV === "production";

module.exports = {
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 2015,
    ecmaFeatures: {
      impliedStrict: true,
      jsx: true
    }
  },
  extends: "react-app",
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error"
  },
  settings: {
    "import/parser": "babel-eslint",
    "import/resolve": {
      moduleDirectory: ["node_modules", "src"]
    }
  },
  globals: {
    window: true,
    document: true,
    requestAnimationFrame: true,
    cancelAnimationFrame: true,
    before: true,
    page: true,
    jasmine: true
  }
};
